package starter.status;
import java.util.List;
import java.util.Map;

import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import io.restassured.response.Response;
import io.restassured.path.json.JsonPath;
import org.junit.Assert;

import static org.assertj.core.api.Assertions.*;

import static starter.WebServiceEndPoints.STATUS;
import static starter.WebServiceEndPoints.TRADE;

public class APIStatus {

    @Step("Check API Current Status")
    public AppStatus currentStatus() {

        int statusCode = SerenityRest.given()
                .contentType("application/json")
                .header("x-rapidapi-key", "3f25ec89dbmsh8046573d6695803p11083ejsn50d6c5e53ea5")
                .header("x-rapidapi-host", "community-open-weather-map.p.rapidapi.com")
                .get(STATUS.getUrl()).statusCode();

        return (statusCode == 200) ? AppStatus.RUNNING : AppStatus.DOWN;
    }

    @Step("Get current status of Weather")
    public String checkWeather(String country) {

        Response res  = SerenityRest.given()
                .contentType("application/json")
                .header("x-rapidapi-key", "3f25ec89dbmsh8046573d6695803p11083ejsn50d6c5e53ea5")
                .header("x-rapidapi-host", "community-open-weather-map.p.rapidapi.com")
                .queryParam("q", country)
                .when().get(TRADE.getUrl())
                .then().contentType(ContentType.JSON).extract().response();

        Map<String, String> weather = res.jsonPath().getMap("weather[0]");
        return weather.get("main");
    }
}
