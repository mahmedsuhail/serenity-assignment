package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;
import starter.status.APIStatus;
import static org.assertj.core.api.Assertions.assertThat;

import static starter.status.AppStatus.RUNNING;


public class WeatherInformationStepDefinitions {
    String weather;

    @Steps
    APIStatus theAPI;

    @Given("the weather map API is working")
    public void the_weather_map_API_is_running()
    {
        assertThat(theAPI.currentStatus()).isEqualTo(RUNNING);
    }

    @When("I check Dubai weather")
    public void i_check_Dubai_weather()
    {
        weather = theAPI.checkWeather("Dubai");
    }

    @Then("the API should return {string}")
    public void the_API_should_return(String expectedWeather)
    {
        assertThat(weather).isEqualTo(expectedWeather);
    }
}
