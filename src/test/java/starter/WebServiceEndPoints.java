package starter;

public enum WebServiceEndPoints {
    STATUS("https://movie-database-imdb-alternative.p.rapidapi.com/weather?q=Dubai"),
    TRADE("https://movie-database-imdb-alternative.p.rapidapi.com/weather");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
