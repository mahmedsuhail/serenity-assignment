Feature: Get Weather Information

  The `https://movie-database-imdb-alternative.p.rapidapi.com/weather` end point returns
  a weather information of any Country name provided as a parameter

  Scenario: Application status end-point
    Given the weather map API is working
    When I check Dubai weather
    Then the API should return "Clear"